#include "market.h"

Market::Market(QObject *parent)
    : QObject(parent)
{
    p_high = 0;
    p_low = 0;
    p_avg = 0;
    p_volumen = 0;
    p_lastBid = 0;
    p_lastAsk = 0;
    p_bestBid = 0;
    p_bestAsk = 0;
    p_tradesBid.clear();
    p_tradesAsk.clear();
    p_ordersBid.clear();
    p_ordersAsk.clear();
}
