#-------------------------------------------------
#
# Project created by QtCreator 2013-03-29T13:41:34
#
#-------------------------------------------------

QT       -= gui

TARGET = market
TEMPLATE = lib

DEFINES += MARKET_LIBRARY

SOURCES += market.cpp

HEADERS += market.h\
        market_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

DESTDIR = ../../DKBitcoinTray
