#ifndef MARKET_GLOBAL_H
#define MARKET_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MARKET_LIBRARY)
#  define MARKETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MARKETSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // MARKET_GLOBAL_H
