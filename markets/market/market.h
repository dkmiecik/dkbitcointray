#ifndef MARKET_H
#define MARKET_H

#include <QObject>
#include <QDateTime>
#include "market_global.h"

class MARKETSHARED_EXPORT Market : public QObject
{
    Q_OBJECT
public:

    enum TradeType {
        TradeAsk = 1,
        TradeBid = 2
    };

    class Order {
    public:
        qreal price;
        qreal amount;
    };

    class Trade {
    public:
        qreal price;
        qreal amount;
        QDateTime date;
        TradeType type;
        uint id;
        qlonglong time;

        bool operator<(const Trade &other) const {
            return time > other.time;
        }
        bool operator==(const Trade &other) const {
            return time == other.time;
        }
    };

    Market(QObject *parent = 0);
    virtual void start() = 0;
    virtual void stop() = 0;

    virtual qreal getHigh() {return p_high;}
    virtual qreal getLow() {return p_low;}
    virtual qreal getAvg() {return p_avg;}
    virtual qreal getVolumen() {return p_volumen;}

    virtual qreal getLastBID() {return p_lastBid;}
    virtual qreal getLastASK() {return p_lastAsk;}

    virtual qreal getBestBID() {return p_bestBid;}
    virtual qreal getBestASK() {return p_bestAsk;}

    virtual QList<Order> getOrdersBID() {return p_ordersBid;}
    virtual QList<Order> getOrdersASK() {return p_ordersAsk;}

    virtual QList<Trade> getTradesBID() {return p_tradesBid;}
    virtual QList<Trade> getTradesASK() {return p_tradesAsk;}

    virtual QString currencySuffix() {return QString("zł");}
    virtual QString currencyName() {return QString("PLN");}

    virtual QDateTime lastUpdate() {return p_lastUpdate;}

    virtual void setUpdateSpeed(uint) = 0;
    virtual uint getUpdateSpeed() = 0;

signals:
    void refreshed();

protected:
    qreal p_high;
    qreal p_low;
    qreal p_avg;
    qreal p_volumen;
    qreal p_lastBid;
    qreal p_lastAsk;
    qreal p_bestBid;
    qreal p_bestAsk;
    QDateTime p_lastUpdate;
    QList<Trade> p_tradesBid;
    QList<Trade> p_tradesAsk;
    QList<Order> p_ordersBid;
    QList<Order> p_ordersAsk;
};

#endif // MARKET_H
