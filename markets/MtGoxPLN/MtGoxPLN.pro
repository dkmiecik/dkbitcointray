#-------------------------------------------------
#
# Project created by QtCreator 2013-03-30T00:10:01
#
#-------------------------------------------------

QT       += core network

TARGET = MtGoxPLN
TEMPLATE = lib
CONFIG += plugin dll

LIBS += ../../DKBitcoinTray/market.dll
INCLUDEPATH += ../market

LIBS += ../../../DKBitcoinTray/lib/libqjson.dll
INCLUDEPATH += ../../lib/Headers

DESTDIR = $$[QT_INSTALL_PLUGINS]/sqldrivers

SOURCES += mtgoxpln.cpp

HEADERS += mtgoxpln.h
OTHER_FILES += \
    metadata.json

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

DESTDIR = ../../DKBitcoinTray/markets
