#include "mtgoxpln.h"
#include <QMap>
#include "parser.h"

MtGoxPLN::MtGoxPLN(QObject *parent) :
    Market(parent)
{
    p_nam = new QNetworkAccessManager(this);
    connect(p_nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(namFinished(QNetworkReply*)));

    p_refreshTimer = new QTimer;
    p_refreshTimer->setInterval(5000);
    p_refreshTimer->setSingleShot(false);
    connect(p_refreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));
}

MtGoxPLN::~MtGoxPLN()
{
    delete p_nam;
    delete p_refreshTimer;
}

void MtGoxPLN::start()
{
    p_refreshTimer->start();
    refresh();
}

void MtGoxPLN::stop()
{
    p_refreshTimer->stop();
}

void MtGoxPLN::refresh()
{
    static uint i = 0;

    if (i == 0)
    {
        p_nam->get(QNetworkRequest(QUrl("http://data.mtgox.com/api/1/BTCPLN/ticker")));
    } else if (i == 1) {
        p_nam->get(QNetworkRequest(QUrl("http://data.mtgox.com/api/1/BTCPLN/depth/fetch")));
    } else if (i == 2) {
        time = QDateTime::currentDateTime().toTime_t();

        p_nam->get(QNetworkRequest(QUrl("https://data.mtgox.com/api/1/BTCPLN/trades")));
        i = 0;
        return;
    }
    i++;
}

void MtGoxPLN::namFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        // TICKER
        if (reply->url().toString() == "http://data.mtgox.com/api/1/BTCPLN/ticker")
        {
            QByteArray ret(reply->readAll());
            QJson::Parser parser;
            bool ok = false;
            QVariantMap result = parser.parse(ret, &ok).toMap();
            if (ok)
            {
                if (result["result"].toString() != "success")
                    return;

                QVariantMap res1 = result["return"].toMap();

                QVariantMap resHigh = res1["high"].toMap();
                p_high = resHigh["value"].toReal();

                QVariantMap resLow = res1["low"].toMap();
                p_low = resLow["value"].toReal();

                QVariantMap resAvg = res1["avg"].toMap();
                p_avg = resAvg["value"].toReal();

                QVariantMap resVol = res1["vol"].toMap();
                p_volumen = resVol["value"].toReal();

                QVariantMap resBuy = res1["buy"].toMap();
                p_bestBid = resBuy["value"].toReal();

                QVariantMap resAsk = res1["sell"].toMap();
                p_bestAsk = resAsk["value"].toReal();

                p_lastUpdate = QDateTime::fromMSecsSinceEpoch(res1["now"].toLongLong());

                emit refreshed();
            }
        } else
        // ORDERBOOK
        if (reply->url().toString() == "http://data.mtgox.com/api/1/BTCPLN/depth/fetch")
        {
            QByteArray ret(reply->readAll());
            QJson::Parser parser;
            bool ok = false;
            QVariantMap result = parser.parse(ret, &ok).toMap();
            if (ok)
            {
                p_ordersBid.clear();
                p_ordersAsk.clear();

                if (result["result"].toString() != "success")
                    return;

                QVariantMap res1 = result["return"].toMap();

                QList<QVariant> vlb = res1["bids"].toList();
                QList<QVariant> vla = res1["asks"].toList();

                for (int i = vlb.size()-1; i >= 0; i--)
                {
                    QVariantMap l = vlb[i].toMap();
                    Order ord;
                    ord.amount = l["amount"].toReal();
                    ord.price = l["price"].toReal();
                    p_ordersBid << ord;
                }

                for (int i = 0; i < vla.size(); i++)
                {
                    QVariantMap l = vla[i].toMap();
                    Order ord;
                    ord.amount = l["amount"].toReal();
                    ord.price = l["price"].toReal();
                    p_ordersAsk << ord;
                }

                emit refreshed();
            }
        } else
        // TRADES
        if (reply->url().toString() == "https://data.mtgox.com/api/1/BTCPLN/trades" )
        {
            QByteArray ret(reply->readAll());
            QJson::Parser parser;
            bool ok = false;
            QVariantMap result = parser.parse(ret, &ok).toMap();
            if (ok)
            {
                p_tradesBid.clear();
                p_tradesAsk.clear();

                if (result["result"].toString() != "success")
                    return;

                QList<QVariant> res1 = result["return"].toList();

                for (int i = 0; i < res1.size(); i++)
                {
                    QVariantMap vm = res1[i].toMap();
                    Market::Trade ord;
                    ord.amount = vm["amount"].toReal();
                    ord.price = vm["price"].toReal();
                    ord.id = vm["tid"].toUInt();
                    ord.time = vm["date"].toLongLong();
                    ord.date = QDateTime::fromMSecsSinceEpoch(vm["date"].toLongLong()*1000);

                    if (vm["trade_type"].toString() == "ask")
                    {
                        ord.type = Market::TradeAsk;
                        p_tradesAsk << ord;
                    }
                    else
                    {
                        ord.type = Market::TradeBid;
                        p_tradesBid << ord;
                    }
                }

                emit refreshed();
            }
        }

        if (p_tradesBid.size())
            p_lastBid = p_tradesBid[0].price;
        if (p_tradesAsk.size())
            p_lastAsk = p_tradesAsk[0].price;

    }
}

//Q_EXPORT_PLUGIN2(MtGoxPLN, MtGoxPLN);
