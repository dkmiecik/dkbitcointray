#-------------------------------------------------
#
# Project created by QtCreator 2013-03-30T19:08:55
#
#-------------------------------------------------

QT       += core gui

TARGET = test
TEMPLATE = lib
CONFIG += plugin

DESTDIR = $$[QT_INSTALL_PLUGINS]/accessible

SOURCES += accessibleplugin.cpp

HEADERS += accessibleplugin.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
