#ifndef ACCESSIBLEPLUGIN_H
#define ACCESSIBLEPLUGIN_H

#include <QtGui/QAccessiblePlugin>


class AccessiblePlugin : public QAccessiblePlugin
{
    Q_OBJECT
public:
    AccessiblePlugin(QObject *parent = 0);
};

#endif // ACCESSIBLEPLUGIN_H
