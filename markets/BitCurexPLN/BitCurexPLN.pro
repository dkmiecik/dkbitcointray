#-------------------------------------------------
#
# Project created by QtCreator 2013-03-29T13:42:46
#
#-------------------------------------------------

QT       += core network

TARGET = BitCurexPLN
TEMPLATE = lib
CONFIG += plugin dll

LIBS += ../../DKBitcoinTray/market.dll
INCLUDEPATH += ../market

DESTDIR = $$[QT_INSTALL_PLUGINS]/styles

SOURCES += bitcurexpln.cpp

HEADERS += bitcurexpln.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

DESTDIR = ../../DKBitcoinTray/markets

OTHER_FILES += \
    metadata.json
