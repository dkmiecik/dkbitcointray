#ifndef BITCUREXPLN_H
#define BITCUREXPLN_H

#include <QObject>
#include <QMetaType>
#include <QMetaEnum>
#include <QtCore>
#include <QTime>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include "market.h"

class BitCurexPLN : public Market
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "market.BitCurexPLN" FILE "metadata.json")
public:
    BitCurexPLN(QObject *parent = 0);
    ~BitCurexPLN();

    void start();
    void stop();

    void setUpdateSpeed(uint s)
    {
        if (s == 0)
            p_refreshTimer->setInterval(5000);
        else if (s == 1)
            p_refreshTimer->setInterval(30*1000);
        else
            p_refreshTimer->setInterval(2*60*1000);
    }
    uint getUpdateSpeed()
    {
        if (p_refreshTimer->interval() == 5000)
            return 0;
        else if (p_refreshTimer->interval() == 30*1000)
            return 1;
        else
            return 2;
    }

private slots:
    void refresh();
    void namFinished(QNetworkReply*);

private:
    QTimer *p_refreshTimer;
    QNetworkAccessManager *p_nam;
};

#endif // BITCUREXPLN_H
