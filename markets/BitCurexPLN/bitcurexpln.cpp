#include "bitcurexpln.h"
#include <QMap>

BitCurexPLN::BitCurexPLN(QObject *parent) :
    Market(parent)
{
    p_nam = new QNetworkAccessManager(this);
    connect(p_nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(namFinished(QNetworkReply*)));

    p_refreshTimer = new QTimer;
    p_refreshTimer->setInterval(5000);
    p_refreshTimer->setSingleShot(false);
    connect(p_refreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));
}

BitCurexPLN::~BitCurexPLN()
{
    delete p_nam;
    delete p_refreshTimer;
}

void BitCurexPLN::start()
{
    p_refreshTimer->start();
    refresh();
}

void BitCurexPLN::stop()
{
    p_refreshTimer->stop();
}

void BitCurexPLN::refresh()
{
    static uint i = 0;

    if (i == 0)
    {
        p_nam->get(QNetworkRequest(QUrl("https://pln.bitcurex.com/data/ticker.json")));
    } else if (i == 1) {
        p_nam->get(QNetworkRequest(QUrl("https://pln.bitcurex.com/data/trades.json")));
    } else if (i == 2) {
        p_nam->get(QNetworkRequest(QUrl("https://pln.bitcurex.com/data/orderbook.json")));
        i = 0;
        return;
    }
    i++;
}

void BitCurexPLN::namFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        // TICKER
        if (reply->url().toString() == "https://pln.bitcurex.com/data/ticker.json")
        {
            QByteArray ret(reply->readAll());
            QJsonDocument sd = QJsonDocument::fromJson(QString(ret).toUtf8());
            QJsonObject sett2 = sd.object();
            if (!sd.isNull())
            {
                p_high = sett2["high"].toDouble();
                p_low = sett2["low"].toDouble();
                p_avg = sett2["avg"].toDouble();
                p_volumen = sett2["vol"].toDouble();
                p_bestBid = sett2["buy"].toDouble();
                p_bestAsk = sett2["sell"].toDouble();
                p_lastUpdate = QDateTime::fromMSecsSinceEpoch(sett2["time"].toString().toLongLong());

                emit refreshed();
            }
        } else
        // TRADES
        if (reply->url().toString() == "https://pln.bitcurex.com/data/trades.json")
        {
            QByteArray ret(reply->readAll());
            QJsonDocument sd = QJsonDocument::fromJson(QString(ret).toUtf8());
            QJsonArray sett2 = sd.array();
            if (!sd.isNull())
            {
                p_tradesBid.clear();
                p_tradesAsk.clear();

                for (int i = 0; i < sett2.size(); i++)
                {
                    QJsonObject vm = sett2[i].toObject();
                    Market::Trade ord;
                    ord.amount = vm["amount"].toString().toFloat();
                    ord.price = vm["price"].toString().toFloat();
                    ord.id = vm["tid"].toDouble();
                    ord.time = vm["date"].toDouble();
                    ord.date = QDateTime::fromMSecsSinceEpoch(vm["date"].toDouble()*1000);

                    if (vm["type"].toDouble() == 1)
                    {
                        ord.type = Market::TradeAsk;
                        p_tradesAsk << ord;
                    }
                    else
                    {
                        ord.type = Market::TradeBid;
                        p_tradesBid << ord;
                    }
                }

                emit refreshed();
            }
        } else
        // ORDERBOOK
        if (reply->url().toString() == "https://pln.bitcurex.com/data/orderbook.json")
        {
            QByteArray ret(reply->readAll());
            QJsonDocument sd = QJsonDocument::fromJson(QString(ret).toUtf8());
            QJsonObject sett2 = sd.object();
            if (!sd.isNull())
            {
                p_ordersBid.clear();
                p_ordersAsk.clear();

                QJsonArray vlb = sett2["bids"].toArray();
                QJsonArray vla = sett2["asks"].toArray();

                for (int i = 0; i < vlb.size(); i++)
                {
                    QJsonArray l = vlb[i].toArray();
                    Order ord;
                    ord.amount = l[1].toString().toFloat();
                    ord.price = l[0].toString().toFloat();
                    p_ordersBid << ord;
                }

                for (int i = 0; i < vla.size(); i++)
                {
                    QJsonArray l = vla[i].toArray();
                    Order ord;
                    ord.amount = l[1].toString().toFloat();
                    ord.price = l[0].toString().toFloat();
                    p_ordersAsk << ord;
                }

                emit refreshed();
            }
        }

        if (p_tradesBid.size())
            p_lastBid = p_tradesBid[0].price;
        if (p_tradesAsk.size())
            p_lastAsk = p_tradesAsk[0].price;

    }
}
