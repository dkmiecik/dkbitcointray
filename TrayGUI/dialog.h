#ifndef DIALOG_H
#define DIALOG_H

#include <QMainWindow>
#include <QPluginLoader>
#include <QMap>
#include <QSystemTrayIcon>
#include <QSettings>
#include "market.h"
#include "dialogsett.h"
#include "dialogabout.h"
#include "dialogtrades.h"
#include "dialognotify.h"

class Dialog : public QMainWindow
{
    Q_OBJECT
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

public slots:
    void refresh();
    void myClose();
    void saveSettings();
    void showSettDialog();
    void settMarketChanged();
    void trayActivated(QSystemTrayIcon::ActivationReason);
    void syncSettings();
    void watchdog();

protected:
    void closeEvent(QCloseEvent*);
    void watchPrice(qreal *lastNotPrice, qreal price, QString ap);

private:
    Market *market;
    QMap<QString, Market*> markets;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QTimer *watchdogTimer;
    QTimer *oneClickTimer;

    bool p_close;

    DialogSett *settDialog;
    QSettings *settings;

    DialogAbout *about;
    DialogTrades *tradesDialog;
    DialogNotify *notifyDialog;

    bool notEnabled;
    qreal notChangeValue;

};

#endif // DIALOG_H
