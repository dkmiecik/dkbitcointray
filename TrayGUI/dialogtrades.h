#ifndef DIALOGTRADES_H
#define DIALOGTRADES_H

#include <QDialog>
#include "market.h"

namespace Ui {
class DialogTrades;
}

class DialogTrades : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogTrades(QWidget *parent = 0);
    ~DialogTrades();
    void setOrdersAsk(QList<Market::Order>);
    void setOrdersBid(QList<Market::Order>);
    void setTrades(QList<Market::Trade>);
    void setCurrencySuffix(QString);
    void setCurrencyName(QString);

public slots:
    void show();

protected:
    void closeEvent(QCloseEvent*);
    
private:
    Ui::DialogTrades *ui;
    QString suffix;
    QString currencyName;
};

#endif // DIALOGTRADES_H
