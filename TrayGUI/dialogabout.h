#ifndef DIALOGABOUT_H
#define DIALOGABOUT_H

#include <QDialog>

namespace Ui {
class DialogAbout;
}

class DialogAbout : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogAbout(QWidget *parent = 0);
    ~DialogAbout();

public slots:
    void show();

protected:
    void closeEvent(QCloseEvent*);
    
private:
    Ui::DialogAbout *ui;
    QString labelWhenCompiled;
};

#endif // DIALOGABOUT_H
