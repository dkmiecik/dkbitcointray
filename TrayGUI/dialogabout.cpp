#include "dialogabout.h"
#include "ui_dialogabout.h"
#include <QCloseEvent>
#include <QTextBrowser>
#include <QFile>

DialogAbout::DialogAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAbout)
{
    ui->setupUi(this);
    labelWhenCompiled = QString::fromLatin1(__TIME__);
    labelWhenCompiled += " ";
    labelWhenCompiled += QString::fromLatin1(__DATE__);
    ui->labelWhenCompiled->setText(labelWhenCompiled);
    this->setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
}
DialogAbout::~DialogAbout()
{
    delete ui;
}

void DialogAbout::show()
{
    this->setVisible(false);
    this->setVisible(true);
    this->raise();
}

void DialogAbout::closeEvent(QCloseEvent *e)
{
    e->ignore();
    hide();
}
