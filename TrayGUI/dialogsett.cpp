#include "dialogsett.h"
#include "ui_dialogsett.h"
#include <QCloseEvent>

DialogSett::DialogSett(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSett)
{
    ui->setupUi(this);
    connect(ui->comboBoxMarket, SIGNAL(currentIndexChanged(int)), this, SIGNAL(marketChange()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(apply()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));
    this->setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
}

DialogSett::~DialogSett()
{
    delete ui;
}

void DialogSett::setMarketsList(QStringList l)
{
    ui->comboBoxMarket->clear();
    ui->comboBoxMarket->addItems(l);
}

QString DialogSett::getSelectedMarket()
{
    return ui->comboBoxMarket->currentText();
}

void DialogSett::setSelectedMarket(QString s)
{
    for (int i = 0; i < ui->comboBoxMarket->count(); i++)
    {
        if (ui->comboBoxMarket->itemText(i) == s)
        {
            ui->comboBoxMarket->setCurrentIndex(i);
            break;
        }
    }
}

void DialogSett::setIntervalValue(uint i)
{
    ui->comboBoxIntSpeed->setCurrentIndex(i);
}

uint DialogSett::getIntervalValue()
{
    return ui->comboBoxIntSpeed->currentIndex();
}

void DialogSett::setNotificationEnabled(bool b)
{
    ui->checkBoxNot->setChecked(b);
}

bool DialogSett::getNotificationEnabled()
{
    return ui->checkBoxNot->isChecked();
}

void DialogSett::setNotificationChange(qreal v)
{
    ui->spinBoxNot->setValue(v);
}

qreal DialogSett::getNotificationChange()
{
    return ui->spinBoxNot->value();
}

void DialogSett::setCurrencySuffix(QString s)
{
    ui->spinBoxNot->setSuffix(s);
}

void DialogSett::show()
{
    this->setVisible(false);
    this->setVisible(true);
    this->raise();
}

void DialogSett::closeEvent(QCloseEvent *e)
{
    e->ignore();
    hide();
}

void DialogSett::apply()
{
    emit settingsChanged();
    hide();
}

void DialogSett::cancel()
{
    hide();
}
