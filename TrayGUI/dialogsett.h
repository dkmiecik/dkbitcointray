#ifndef DIALOGSETT_H
#define DIALOGSETT_H

#include <QDialog>
#include <QStringList>

namespace Ui {
class DialogSett;
}

class DialogSett : public QDialog
{
    Q_OBJECT
public:
    explicit DialogSett(QWidget *parent = 0);
    ~DialogSett();

    void setMarketsList(QStringList);
    QString getSelectedMarket();
    void setSelectedMarket(QString);

    void setIntervalValue(uint);
    uint getIntervalValue();

    void setNotificationEnabled(bool);
    bool getNotificationEnabled();
    void setNotificationChange(qreal);
    qreal getNotificationChange();

    void setCurrencySuffix(QString);

public slots:
    void show();

protected:
    void closeEvent(QCloseEvent*);

signals:
    void settingsChanged();
    void marketChange();

private slots:
    void cancel();
    void apply();
    
private:
    Ui::DialogSett *ui;
};

#endif // DIALOGSETT_H
