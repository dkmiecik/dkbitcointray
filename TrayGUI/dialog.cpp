#include "dialog.h"
#include <QDir>
#include <QPluginLoader>
#include <QDebug>
#include <QMenu>
#include <QCloseEvent>
#include <QTimer>
#include <QUrl>
#include <QMessageBox>
#include <QPixmap>

Dialog::Dialog(QWidget *parent) :
    QMainWindow(parent),
    market(NULL),
    p_close(false)
{
    settings = new QSettings("conf.ini", QSettings::IniFormat);
    watchdogTimer = new QTimer;
    watchdogTimer->setSingleShot(false);
    connect(watchdogTimer, SIGNAL(timeout()), this, SLOT(watchdog()));

    //Create notify dialog
    notifyDialog = new DialogNotify(this);
    oneClickTimer = new QTimer;
    oneClickTimer->setSingleShot(true);
    oneClickTimer->setInterval(700);
    connect(oneClickTimer, SIGNAL(timeout()), notifyDialog, SLOT(show()));

    // Create trade window
    tradesDialog = new DialogTrades(this);

    // Create about window
    about = new DialogAbout(this);

    // Create settings window
    settDialog = new DialogSett(this);
    connect(settDialog, SIGNAL(settingsChanged()), this, SLOT(saveSettings()));
    connect(settDialog, SIGNAL(marketChange()), this, SLOT(settMarketChanged()));
    settDialog->hide();

    // Create system try icon
    trayIcon = new QSystemTrayIcon;
    trayIcon->setIcon(QIcon(":/icons/btc_working.png"));
    trayIcon->setToolTip(tr("Nie podłączono do giełdy bitcoin"));
    trayIcon->show();
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayActivated(QSystemTrayIcon::ActivationReason)));

    // Create menu
    trayIconMenu = new QMenu;
    trayIconMenu->addAction(tr("Rynek"), tradesDialog, SLOT(show()));
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(tr("Ustawienia"), this, SLOT(showSettDialog()));
    trayIconMenu->addAction(tr("O programie"), about, SLOT(show()));
    trayIconMenu->addAction(tr("Wyłącz"), this, SLOT(myClose()));
    trayIcon->setContextMenu(trayIconMenu);

    // Load DLL's
    QString defaultMarket;
    QDir pluginsDir = QDir("markets");
    foreach(QString fileName, pluginsDir.entryList(QDir::Files))
    {
        QPluginLoader *loader = new QPluginLoader(pluginsDir.absoluteFilePath(fileName));
        loader->setLoadHints(QLibrary::ResolveAllSymbolsHint);
        QObject *plugin = loader->instance();
        if (plugin && dynamic_cast<Market*>(plugin))
        {
            Market *_market = dynamic_cast<Market*>(plugin);
            QString name = _market->metaObject()->className();
            qDebug() << "Loaded " << _market->metaObject()->className();
            markets[name] = _market;
            defaultMarket = name;
        }
    }
    if (markets.isEmpty())
    {
        QMessageBox::critical(this, this->windowTitle()+tr(" - Błąd"), tr("Nieprawidłowa instalacja programu!"));
        myClose();
    }

    // Read settings
    this->setGeometry(settings->value("main/geometry", QRect(0, 0, 0, 0)).toRect());
    settDialog->setGeometry(settings->value("sett/geometry", QRect(17, 35, 313, 235)).toRect());
    tradesDialog->setGeometry(settings->value("trades/geometry", QRect(11, 36, 750, 400)).toRect());
    about->setGeometry(settings->value("about/geometry", QRect(14, 36, 402, 306)).toRect());
    if (markets.contains(settings->value("market/market", defaultMarket).toString()))
    {
        market = markets[settings->value("market/market", defaultMarket).toString()];
        market->start();
        connect(market, SIGNAL(refreshed()), this, SLOT(refresh()));
    }
    if (market)
    {
        market->setUpdateSpeed(settings->value("market/interval", 1).toUInt());
        about->setWindowTitle( tr("O programie") + " - " + market->metaObject()->className() );
        settDialog->setWindowTitle( tr("Ustawienia") + " - " + market->metaObject()->className() );
        tradesDialog->setWindowTitle( tr("Rynek") + " - " + market->metaObject()->className() );
    }
    notChangeValue = settings->value("main/notificationChange", 5.0).toReal();
    notEnabled = settings->value("main/notification", false).toBool();

    // Hide main window
    hide();

    watchdogTimer->start(30000 * (1 + market->getUpdateSpeed()));
}

Dialog::~Dialog()
{
    delete notifyDialog;
    delete oneClickTimer;
    delete about;
    delete settings;
    delete settDialog;
    delete tradesDialog;
    delete trayIcon;
    delete trayIconMenu;
    if (market)
        delete market;
}

void Dialog::refresh()
{
    QString toolTip;
//    toolTip = tr("Kantor: ");
//    toolTip += market->metaObject()->className();
    toolTip += tr("Najlepszy skup:       ");
    toolTip += QString::number(market->getBestBID(), 'f', 2) + market->currencySuffix();
    toolTip += tr("\nNajlepsza sprzedaż: ");
    toolTip += QString::number(market->getBestASK(), 'f', 2) + market->currencySuffix();
//    toolTip += tr("\nOstatnia tranzakcja: ");
//    toolTip += QString::number(market->getLastBID(), 'f', 2) + market->currencySuffix();
//    toolTip += tr(" / ");
//    toolTip += QString::number(market->getLastASK(), 'f', 2) + market->currencySuffix();
    trayIcon->setToolTip(toolTip);

    tradesDialog->setOrdersAsk(market->getOrdersASK());
    tradesDialog->setOrdersBid(market->getOrdersBID());
    tradesDialog->setTrades(market->getTradesASK()+market->getTradesBID());
    tradesDialog->setCurrencySuffix(market->currencySuffix());
    tradesDialog->setCurrencyName(market->currencyName());

    qreal sbid = settings->value("notification/sbid", 100).toReal();
    watchPrice(&sbid, market->getBestBID(), tr("skupu"));
    settings->setValue("notification/sbid", sbid);

    qreal sask = settings->value("notification/sask", 100).toReal();
    watchPrice(&sask, market->getBestASK(), tr("sprzedaży"));
    settings->setValue("notification/sask", sask);

    trayIcon->setIcon(QIcon(":/icons/btc_notworking.png"));
    watchdogTimer->start(30000 * (1 + market->getUpdateSpeed()));
}

void Dialog::myClose()
{
    p_close = true;
    syncSettings();
    close();
}

void Dialog::saveSettings()
{
    if (market)
    {
        market->stop();
        disconnect(market, SIGNAL(refreshed()), this, SLOT(refresh()));
    }
    market = markets[settDialog->getSelectedMarket()];
    market->start();
    connect(market, SIGNAL(refreshed()), this, SLOT(refresh()));
    market->setUpdateSpeed(settDialog->getIntervalValue());

    about->setWindowTitle( tr("O programie") + " - " + market->metaObject()->className() );
    settDialog->setWindowTitle( tr("Ustawienia") + " - " + market->metaObject()->className() );
    tradesDialog->setWindowTitle( tr("Rynek") + " - " + market->metaObject()->className() );

    notEnabled = settDialog->getNotificationEnabled();
    notChangeValue = settDialog->getNotificationChange();

    syncSettings();
}

void Dialog::showSettDialog()
{
    QStringList l;
    QMapIterator<QString, Market*> i(markets);
    while (i.hasNext())
    {
        i.next();
        l << i.key();
    }

    settDialog->setMarketsList(l);
    settDialog->setNotificationChange(notChangeValue);
    settDialog->setNotificationEnabled(notEnabled);
    if (market)
    {
        settDialog->setIntervalValue(market->getUpdateSpeed());
        settDialog->setSelectedMarket(market->metaObject()->className());
    }
    settDialog->show();
}

void Dialog::settMarketChanged()
{
    QString name = settDialog->getSelectedMarket();
    if (markets.contains(name))
    {
        settDialog->setCurrencySuffix(markets[name]->currencySuffix());
    }
}

void Dialog::trayActivated(QSystemTrayIcon::ActivationReason r)
{
    if (r == QSystemTrayIcon::DoubleClick)
    {
        if (tradesDialog->isVisible())
            tradesDialog->hide();
        else
            tradesDialog->show();
        oneClickTimer->stop();
    } else if (r == QSystemTrayIcon::Trigger) {
//        oneClickTimer->start();
//        QPoint p = trayIcon->geometry().topLeft();
//        notifyDialog->setGeometry(QRect(p.x()-125, p.y()-110, 250, 100));
    }
}

void Dialog::syncSettings()
{
    // Save settings
    settings->setValue("market/market", market->metaObject()->className());
    settings->setValue("main/notification", notEnabled);
    settings->setValue("main/notificationChange", notChangeValue);
    settings->setValue("market/interval", market->getUpdateSpeed());
    settings->setValue("main/geometry", this->geometry());
    settings->setValue("sett/geometry", settDialog->geometry());
    settings->setValue("trades/geometry", tradesDialog->geometry());
    settings->setValue("about/geometry", about->geometry());
}

void Dialog::watchdog()
{
    trayIcon->setIcon(QIcon(":/icons/btc_working.png"));
}

void Dialog::watchPrice(qreal *lastNotPrice, qreal price, QString ap)
{


    if (notEnabled)
    {
         if (price > *lastNotPrice+notChangeValue)
         {
            QString msg;
            msg = tr("Wzrost ceny %1 o ").arg(ap);
            msg += QString::number(price-*lastNotPrice, 'f', 2) + market->currencySuffix() + "\n";
            msg += tr("Poprzednia cena: ") + QString::number(*lastNotPrice, 'f', 2) + market->currencySuffix() + "\n";
            msg += tr("Aktualna cena: ") + QString::number(price, 'f', 2) + market->currencySuffix();

            QPoint p = trayIcon->geometry().topLeft();
//            trayIcon->showMessage(market->metaObject()->className(), msg, QSystemTrayIcon::Critical, 0xFFFF);
//            notifyDialog->setGeometry(screenWidth-notifyDialog->width(),110+notifyDialog->height(),notifyDialog->width(),notifyDialog->height());
            notifyDialog->setGeometry(QRect(p.x()-0, p.y()-110, 250, 100));
            QPixmap icon(":/icons/Curs_down.png");
            notifyDialog->showMessage(icon,msg,10000);

            *lastNotPrice = price;
        }
        if (price < *lastNotPrice-notChangeValue)
        {
            QString msg;
            msg = tr("Spadek ceny %1 o ").arg(ap);
            msg += QString::number(*lastNotPrice-price, 'f', 2) + market->currencySuffix() + "\n";
            msg += tr("Poprzednia cena: ") + QString::number(*lastNotPrice, 'f', 2) + market->currencySuffix() + "\n";
            msg += tr("Aktualna cena: ") + QString::number(price, 'f', 2) + market->currencySuffix();

//            trayIcon->showMessage(market->metaObject()->className(), msg, QSystemTrayIcon::Critical, 0xFFFF);

            QPoint p = trayIcon->geometry().topLeft();
            notifyDialog->setGeometry(QRect(p.x()-0, p.y()-110, 250, 100));
            QPixmap icon(":/icons/Curs_down.png");
            notifyDialog->showMessage(icon,msg,10000);

            *lastNotPrice = price;
        }
    }
}

void Dialog::closeEvent(QCloseEvent *e)
{
    if (!p_close)
        e->ignore();
    hide();
}
