#include "dialogtrades.h"
#include "ui_dialogtrades.h"
#include <QCloseEvent>
#include <QtAlgorithms>

DialogTrades::DialogTrades(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTrades)
{
    ui->setupUi(this);
    this->setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
}

DialogTrades::~DialogTrades()
{
    delete ui;
}

void DialogTrades::setOrdersAsk(QList<Market::Order> q)
{
    ui->tableWidgetOrdersAsk->clear();
    ui->tableWidgetOrdersAsk->setRowCount(q.count());
    ui->tableWidgetOrdersAsk->setColumnCount(3);
    QStringList l;
    l << tr("Kurs (%1)").arg(currencyName) << tr("Ilość BTC") << tr("Cena (%1)").arg(currencyName);
    ui->tableWidgetOrdersAsk->setHorizontalHeaderLabels(l);

    for(int i=0;i<q.count();i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(q[i].price, 'f', 2)+suffix);
        newItem->setBackgroundColor(QColor(255,220,220));
        ui->tableWidgetOrdersAsk->setItem(i, 0, newItem);
        QTableWidgetItem *newItem1 = new QTableWidgetItem(QString::number(q[i].amount, 'f', 10));
        newItem1->setBackgroundColor(QColor(255,220,220));
        ui->tableWidgetOrdersAsk->setItem(i, 1, newItem1);
        QTableWidgetItem *newItem2 = new QTableWidgetItem(QString::number(q[i].price*q[i].amount, 'f', 2)+suffix);
        newItem2->setBackgroundColor(QColor(255,220,220));
        ui->tableWidgetOrdersAsk->setItem(i, 2, newItem2);
    }
}

void DialogTrades::setOrdersBid(QList<Market::Order> q)
{
    ui->tableWidgetOrdersBid->clear();
    ui->tableWidgetOrdersBid->setRowCount(q.count());
    ui->tableWidgetOrdersBid->setColumnCount(3);
    QStringList l;
    l << tr("Kurs (%1)").arg(currencyName) << tr("Ilość BTC") << tr("Cena (%1)").arg(currencyName);
    ui->tableWidgetOrdersBid->setHorizontalHeaderLabels(l);

    for(int i=0;i<q.count();i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(q[i].price, 'f', 2)+suffix);
        newItem->setBackgroundColor(QColor(220,255,225));
        ui->tableWidgetOrdersBid->setItem(i, 0, newItem);
        QTableWidgetItem *newItem1 = new QTableWidgetItem(QString::number(q[i].amount, 'f', 10));
        newItem1->setBackgroundColor(QColor(220,255,225));
        ui->tableWidgetOrdersBid->setItem(i, 1, newItem1);
        QTableWidgetItem *newItem2 = new QTableWidgetItem(QString::number(q[i].price*q[i].amount, 'f', 2)+suffix);
        newItem2->setBackgroundColor(QColor(220,255,225));
        ui->tableWidgetOrdersBid->setItem(i, 2, newItem2);
    }
}

void DialogTrades::setTrades(QList<Market::Trade> t)
{
    qSort(t.begin(), t.end());

    ui->tableWidgetTrades->clear();
    ui->tableWidgetTrades->setRowCount(t.count());
    ui->tableWidgetTrades->setColumnCount(4);
    QStringList l;
    l << tr("Kurs (%1)").arg(currencyName) << tr("Ilość BTC") << tr("Cena (%1)").arg(currencyName) << tr("Data");
    ui->tableWidgetTrades->setHorizontalHeaderLabels(l);

    for(int i=0;i<t.count();i++)
    {

        if(t[i].type == Market::TradeAsk)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(t[i].price, 'f', 2)+suffix);
            newItem->setBackgroundColor(QColor(255,220,220));
            ui->tableWidgetTrades->setItem(i, 0, newItem);
            QTableWidgetItem *newItem1 = new QTableWidgetItem(QString::number(t[i].amount, 'f', 10));
            newItem1->setBackgroundColor(QColor(255,220,220));
            ui->tableWidgetTrades->setItem(i, 1, newItem1);
            QTableWidgetItem *newItem2 = new QTableWidgetItem(QString::number(t[i].price*t[i].amount, 'f', 2)+suffix);
            newItem2->setBackgroundColor(QColor(255,220,220));
            ui->tableWidgetTrades->setItem(i, 2, newItem2);
            QTableWidgetItem *newItem3 = new QTableWidgetItem(t[i].date.toString("dd.MM.yyyy hh:mm:ss"));
            newItem3->setBackgroundColor(QColor(255,220,220));
            ui->tableWidgetTrades->setItem(i, 3, newItem3);
        }
        else
        {
            QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(t[i].price, 'f', 2)+suffix);
            newItem->setBackgroundColor(QColor(220,255,225));
            ui->tableWidgetTrades->setItem(i, 0, newItem);
            QTableWidgetItem *newItem1 = new QTableWidgetItem(QString::number(t[i].amount, 'f', 10));
            newItem1->setBackgroundColor(QColor(220,255,225));
            ui->tableWidgetTrades->setItem(i, 1, newItem1);
            QTableWidgetItem *newItem2 = new QTableWidgetItem(QString::number(t[i].price*t[i].amount, 'f', 2)+suffix);
            newItem2->setBackgroundColor(QColor(220,255,225));
            ui->tableWidgetTrades->setItem(i, 2, newItem2);
            QTableWidgetItem *newItem3 = new QTableWidgetItem(t[i].date.toString("dd.MM.yyyy hh:mm:ss"));
            newItem3->setBackgroundColor(QColor(220,255,225));
            ui->tableWidgetTrades->setItem(i, 3, newItem3);
        }
    }
}

void DialogTrades::setCurrencySuffix(QString q)
{
    suffix = q;
}

void DialogTrades::setCurrencyName(QString q)
{
    currencyName = q;
}

void DialogTrades::show()
{
    this->setVisible(false);
    this->setVisible(true);
    this->raise();
}

void DialogTrades::closeEvent(QCloseEvent *e)
{
    e->ignore();
    hide();
}
