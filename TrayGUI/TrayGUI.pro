#-------------------------------------------------
#
# Project created by QtCreator 2013-03-29T13:43:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

LIBS += ../DKBitcoinTray/market.dll
INCLUDEPATH += ../markets/market

TARGET = DKBitcoinTray
TEMPLATE = app

SOURCES += main.cpp\
        dialog.cpp \
    dialogsett.cpp \
    dialogabout.cpp \
    dialogtrades.cpp \
    dialognotify.cpp

HEADERS  += dialog.h \
    dialogsett.h \
    dialogabout.h \
    dialogtrades.h \
    dialognotify.h

FORMS    += \
    dialogsett.ui \
    dialogabout.ui \
    dialogtrades.ui \
    dialognotify.ui

win32:RC_FILE = Win.rc

OTHER_FILES += \
    Win.rc

OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

DESTDIR = ../DKBitcoinTray

RESOURCES += \
    res.qrc
