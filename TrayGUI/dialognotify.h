#ifndef DIALOGNOTIFY_H
#define DIALOGNOTIFY_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class DialogNotify;
}

class DialogNotify : public QDialog
{
    Q_OBJECT
public:
    explicit DialogNotify(QWidget *parent = 0);
    ~DialogNotify();

    void showMessage(const QString &msg, uint ms = 10000);
    void showMessage(const QPixmap &pixmap, const QString &msg, uint ms = 10000);

public slots:
    void show();
    void emergLeave();
    void closeMsg();

protected:
    void closeEvent(QCloseEvent*);
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);
    void mousePressEvent(QMouseEvent*);
    
private:
    Ui::DialogNotify *ui;
    QTimer *hideTimer;
    QTimer *hide2Timer;
    QTimer *closeTimer;
};

#endif // DIALOGNOTIFY_H
