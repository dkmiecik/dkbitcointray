#include "dialognotify.h"
#include "ui_dialognotify.h"
#include <QCloseEvent>
#include <QDebug>
#include <QImage>

DialogNotify::DialogNotify(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogNotify)
{
    ui->setupUi(this);
    this->setWindowFlags(windowFlags() | Qt::ToolTip | Qt::WindowStaysOnTopHint);
    setStyleSheet(
        "QDialog {border: 1px solid black;}"
    );
    setWindowOpacity(0.90);
    hideTimer = new QTimer;
    hideTimer->setInterval(300);
    hideTimer->setSingleShot(true);
    connect(hideTimer, SIGNAL(timeout()), this, SLOT(show()));
    hide2Timer = new QTimer;
    hide2Timer->setInterval(300);
    hide2Timer->setSingleShot(true);
    connect(hide2Timer, SIGNAL(timeout()), this, SLOT(emergLeave()));
    closeTimer = new QTimer;
    closeTimer->setInterval(10000);
    closeTimer->setSingleShot(true);
    connect(closeTimer, SIGNAL(timeout()), this, SLOT(closeMsg()));
}

DialogNotify::~DialogNotify()
{
    delete closeTimer;
    delete hideTimer;
    delete hide2Timer;
    delete ui;
}

void DialogNotify::showMessage(const QString &msg, uint ms)
{
    QImage image(":/icons/btc_icon.png");
    image = image.scaledToWidth(48);
    showMessage(QPixmap::fromImage(image), msg, ms);
}

void DialogNotify::showMessage(const QPixmap &pixmap, const QString &msg, uint ms)
{
    ui->labelImg->setPixmap(pixmap);
    ui->labelMsg->setText(msg);
    closeTimer->setInterval(ms);
    closeTimer->start();
    show();
}

void DialogNotify::show()
{
    this->setVisible(false);
    this->setVisible(true);
    this->raise();
}

void DialogNotify::emergLeave()
{
    leaveEvent(NULL);
}

void DialogNotify::closeMsg()
{
    hide();
    hideTimer->stop();
    hide2Timer->stop();
}

void DialogNotify::closeEvent(QCloseEvent *e)
{
    e->ignore();
    hide();
}

void DialogNotify::enterEvent(QEvent*)
{
    this->setVisible(false);
    hideTimer->stop();
    hide2Timer->start();
}

void DialogNotify::leaveEvent(QEvent*)
{
    QPoint mp = QCursor::pos();
    if (geometry().contains(mp) == false)
    {
        hideTimer->start();
        hide2Timer->stop();
    } else {
        hide2Timer->start();
    }
}

void DialogNotify::mousePressEvent(QMouseEvent *)
{
}
